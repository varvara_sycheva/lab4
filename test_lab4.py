import unittest
from lab4_v20 import check_int
from lab4_v20 import check_str
from lab4_v20 import find_winner

class ImportTest(unittest.TestCase):
    def test_check_int(self):
        """
        Тест функции проверки ввода количества игроков.

        Тетстируется два случая: верный ввод и ввод с ошибкой.
        """
        self.assertEqual(check_int('str'), 1)
        self.assertEqual(check_int(3), 0)
    def test_check_str(self):
        """
        Тест функции проверки ввода игровых команд.

        Тетстируется два случая: верный ввод и ввод с ошибкой.
        """
        self.assertEqual(check_str('rtty', 0), 1)
        self.assertEqual(check_str('rock', 0), 0)
    def test_find_winner(self):
        """
        Тест функции определения победителя.

        Тетстируется два случая: с возможностью определния победителя
        и с ситуацией, кога победителя победить невозможно.
        """
        self.assertEqual(find_winner(3, ['rock', 'scissors', 'paper']), 0)
        self.assertEqual(find_winner(4, ['rock', 'rock', 'paper', 'rock']), 3)

unittest.main()
