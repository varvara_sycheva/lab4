import sys


def check_int(test_input):
    """
    Функция проверки ввода количества игроков.

    Принимает на вход аргумент типа str, затем проверяет является ли он числом

    Args:
        test_input: введенный пользователем аргумент

    Returns:
        0, если аргумент является натуральным числом
        1, если агрумент не является натуральным числом

    Exception:
        ValueError

    Examples:
        >>> check_int(3)
        (0)
        >>> check_int(abc)
        (1)
    """
    control = 0
    try:
        check = int(test_input)
    except ValueError:
        control += 1
    return control


def check_str(test_str):
    """
    Функция проверки ввода игровых команд.

    Принимает на вход аргумент типа str, затем проверяет соответствует ли он одной
    из игровых команд.

    Args:
        test_str: введенная пользователем команда

    Returns:
        0, если команда соответстует игровой
        1, если команда не соответствует игровой

    Examples:
        >>> check_str(3)
        (1)
        >>> check_str(rock)
        (0)
    """
    control = 0
    rock = 'rock'
    scissors = 'scissors'
    paper = 'paper'
    if (test_str == rock) or (test_str == scissors) or (test_str == paper):
        control = 0
    else:
        control += 1
    return control


def find_winner(num_of_moves, moves):
    """
    Функция определения победителя игры.

    Принимает на вход два аргумента: количество игроков и их ходы,
    затем проверяет существует ли победитель, и определяет его, если да

    Args:
        num_of_moves: количество игроков
        moves: команды каждого из игроков

    Returns:
        0, если победителя определить невозможно
        номер победившего игрока, если победитель был определен

    Examples:
        >>> find_winner(3, [rock, scissors, paper])
        (0)
        >>> find_winner(3, [paper, scissors, paper])
        (2)
    """
    rock = 0
    paper = 0
    scissors = 0
    for i in range(num_of_moves):
        if len(moves[i]) == 8:
            scissors += 1
            player_scissors = i + 1
        elif len(moves[i]) == 4:
            rock += 1
            player_rock = i + 1
        else:
            paper += 1
            player_paper = i + 1
    if (scissors != 0) and (rock != 0) and (paper != 0):
        return 0
    elif scissors == 0:
        if (rock > 0) and (paper == 1):
            return player_paper
        else:
            return 0
    elif rock == 0:
        if (scissors == 1) and (paper > 0):
            return player_scissors
        else:
            return 0
    elif paper == 0:
        if (scissors > 0) and (rock == 1):
            return player_rock
        else:
            return 0


def main():

    ctrl_num = 0
    players_num = 0

    while ctrl_num == 0:
        players_num = input('input number of players: ')
        check = check_int(players_num)
        if check > 0:
            print('error: wrong input')
            continue
        break
    players_num = int(players_num)

    players_moves = []
    for i in range(players_num):
        while ctrl_num == 0:
            curr_move = input('input {} move: '.format(i+1))
            check = check_str(curr_move, 0)
            if check != 0:
                print(' ')
                continue
            else:
                players_moves.append(curr_move)
                break

    winner = find_winner(players_num, players_moves)
    if winner == 0:
        print('Impossible to determinate winner')
    else:
        print('Winner is player number {}'.format(winner))


if __name__ == "__main__":
    sys.exit(main())
